import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { AddPostComponent } from './add-post/add-post.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';

export const AppRoutes: Routes = [
{ path: '', component: LoginComponent, pathMatch: 'full' }, 
{ path: 'home', component: HomeComponent },
{ path: 'add', component: AddPostComponent }
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);