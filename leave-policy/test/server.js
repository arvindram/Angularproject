const express = require('express');
const bodyParser = require('body-parser');
var mongoose = require('mongoose');
const userModal=require('./app/models/user.model.js')
// create express app
const app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())



//Set up default mongoose connection
var mongoDB = 'mongodb://127.0.0.1:27017/leave';
mongoose.connect(mongoDB);
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;



// define a simple route
app.get('/', (req, res) => {
    res.json({"message": "Welcome to EasyNotes application. Take notes quickly. Organize and keep track of all your notes."});
});

app.get('/users', (req, res) => {
	
    res.json({"message": "list of users"});
});

app.post('/createUser', (req, res) => {
   const username = req.body.username;
   const email_id = req.body.email;   
	var user = new userModal({ username: username,email:email_id});
	
	user.save(function (err) {
	  if (err) console.log(err);5
	  // saved!
	  res.json({"message": "user created"});
	});
    
});

// listen for requests
app.listen(8080, () => {
    console.log("Server is listening on port 8080");
});

const Model=mongoose.model('users',mongoose);
app.get('/users1', (req, res) => {
Model.find()
.then(doc => res.send(doc))
.catch(err => res.send(err.message));
// res.json({"message":"SUCCESS"});
});
app.listen(8080, () => {
console.log("Server is listening on port 8080");
});
