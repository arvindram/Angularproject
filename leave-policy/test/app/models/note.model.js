const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    Name: String,
    position: String,
    office: String,
    age:Number,
    Salary:Number
});

module.exports = mongoose.model('Note', NoteSchema);